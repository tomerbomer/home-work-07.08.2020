def get_mySqr(x):
    """ gets number x. returns x squared """
    return x ** 2


def get_myMul(x, y):
    """ Gets numbers x, y. Returns x+y """
    return x + y


def get_factorial(x):
    """ Returns the factorial of x."""
    fact = 1
    for num in range(2, x + 1):
        fact *= num
    return fact


def get_checkEven(x):
    """ Checks if x is even.
    Returns True if even, False otherwise."""
    return True if not x % 2 else False  # not x%2 == x divides by 2 with no remainder


def get_average(list_of_numbers=[0]):
    """ Gets a list of numbers. Returns the average """
    return sum(list_of_numbers) / len(list_of_numbers)

def get_wordConcat(first_word, second_word):
    """ Gets 2 words (strings).
    Returns one string complied by given words with space between them. """
    return first_word + " " + second_word
