def add(x=0, y=0):
    """ Returns the sum of given x, y """
    return x + y

def mul(x=0, y=0):
    """ Retuns the multiply of x by y =(x * y) """
    return x * y

def sub(x=0, y=0):
    """ Returns y subtracted from x =(x - y) """
    return x - y

def div(x=0, y=1):
    """ Returns the division of x by y =(x / y) """
    return x / y

def main():
    x = int(input("Enter first number: "))
    y = int(input("Enter second number: "))
    print("{} + {} = {}".format(x, y, add(x, y)))
    print("{} - {} = {}".format(x, y, sub(x, y)))
    print("{} * {} = {}".format(x, y, mul(x, y)))
    print("{} / {} = {}".format(x, y, div(x, y)))


main()
