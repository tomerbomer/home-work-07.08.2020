def getMin(num1, num2, num3):
    """ Gets 3 numbers and returns the smallest one. """
    numList = [num1, num2, num3]
    numList.sort()
    return numList[0]
