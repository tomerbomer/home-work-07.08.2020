def getInRange(min, max):
    """ Asks for a number from user.
    Runs until user inputs a number within the range from min to max
    and returns that number. """
    while True:
        num = int(input(f"Please enter a number within the range({min}, {max}): "))
        if num in range(min, max + 1):
            return num

