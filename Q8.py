def get_circ(radius):
    """ Gets the radius and returns the circumference. """
    return round(2 * 3.14 * radius, 2)


